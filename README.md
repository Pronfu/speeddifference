# Speed Difference #

This site and each of the pages are meant to show how loading from different CDN's can change the speed that the page loads. And how images affect how big the page size is.

## Credits / Thanks ##

[CDNJS](https://cdnjs.com/) & [JSDelivr](https://www.jsdelivr.com/) for their CDN's. 

[Bootstrap ( v5.0.0-beta2)](https://getbootstrap.com/docs/5.0/getting-started/introduction/) for the framework that this site is using.

There are many other credits in the comments of each page.

## Live Demo ##

[projects.gregoryhammond.ca/speed-difference/](https://projects.gregoryhammond.ca/speed-difference/)

## License ##

[Unlicense](https://unlicense.org/)